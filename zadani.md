# Zadání: Vytvořit Express.js aplikaci v Node.js
1. Vytvořit endpoint na zjištění stavu aplikace (zda běží, vrátí pouze „OK“, „Hello World“, nebo
obdobně)
    - a. Dostupná pro všechny (bez autentizace)
2. Vytvořit autentizaci (přes Basic, x-api-key, x-token, nebo jakkoliv jinak), stačí základní
zabezpečení (zadání hesla nebo tokenu nebo api klíče)
3. Vytvořit zabepečený endpoint pro zjištění stavu a platnosti karty
    - a. Url parametr číslo karty
    - b. Endpoint vrací konec platnosti karty formátovaný ve formátu den.měsíc.rok (např.
„28.2.2020“) a stav karty textově
        - i. URL pro zjištění platnosti karty je:
http://private-264465-litackaapi.apiary-mock.com/cards/{číslo karty}/validity
        - ii. URL pro zjištění stavu karty je:
http://private-264465-litackaapi.apiary-mock.com/cards/{číslo karty}/state
(jedná se o mock data, bude vracet vždy stejnou hodnotu - nevadí)
(dokumentace API zde: https://litackaapi.docs.apiary.io/#reference/0/platnost-cipove-karty-litackaopencard/platnost-cipove-karty)
4. Napsat README s návodem tak, aby bylo možné projekt podle návodu úspěšně spustit
5. (nepovinné) Vytvoření 1-3 základních unit testů
6. (nepovinné) Vytvoření dokumentace API