import app from './app';
import configValues from './config';
// Start the server

app.listen(configValues.port, () => {
    console.log(`Server is running at http://localhost:${configValues.port}`);
});

// server.ts and app.ts are separeted because of tests
// they can test the endpoints without statrting the server
