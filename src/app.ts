import express from 'express';
import routes from './routes/litacka';

// Create Express app and load Routes
const app = express();

app.use('/', routes);

export default app;
