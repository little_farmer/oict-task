import { Request, Response, NextFunction } from 'express';
import { StatusCodes } from 'http-status-codes';

// sample data of users, simple array with objects instead of database
const users = [{ apiKey: '6spr9fydoy9eqknchzeckezy33d74tggiihp' }, { apiKey: 'rwuy6434tgdgjhtiojiosi838tjue3da42gs' }];

export default function userAuthentificationMiddleware(req: Request, res: Response, next: NextFunction) {
    const xApiKey = req.header('X-API-Key');

    if (xApiKey === undefined) {
        const result = {
            error_code: StatusCodes.UNAUTHORIZED,
            error_description: 'Error authenticating provided apikey.',
        };

        const wwwAuthenticateHeader = 'Basic realm="oict-task-api", charset="UTF-8", error="Missing X-API-KEY header" type="apiKey"';
        res.set('WWW-Authenticate', wwwAuthenticateHeader);
        res.status(StatusCodes.UNAUTHORIZED).send(result);
        return;
    }

    const user = users.some((u) => u.apiKey === xApiKey);
    if (!user) {
        const result = {
            error_code: StatusCodes.FORBIDDEN,
            error_description: 'Access is denied to the user.',
        };

        res.status(StatusCodes.FORBIDDEN).send(result);
        return;
    }

    next();
}
