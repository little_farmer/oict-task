import { Request, Response, NextFunction } from 'express';
import { StatusCodes } from 'http-status-codes';
import { ErrorNotFound } from '../services/litackaApiService';

// express is expecting 4 parameters for error handler
// eslint-disable-next-line @typescript-eslint/no-unused-vars
export default (error: Error, request: Request, response : Response, next: NextFunction) => {
    if (error instanceof ErrorNotFound) {
        response.status(StatusCodes.NOT_FOUND).send({
            error_code: StatusCodes.NOT_FOUND,
            error_description: 'Card was not found.',
        });
        return;
    }

    response.status(StatusCodes.INTERNAL_SERVER_ERROR).send({
        error_code: StatusCodes.INTERNAL_SERVER_ERROR,
        error_description: 'Internal server error',
    });
};
