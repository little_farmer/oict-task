// cardId, apiKey, endpoints
import { StatusCodes } from 'http-status-codes';
import configValues from '../config';

interface LitackaState {
    state_id: number;
    state_description: string;
}

interface LitackaValidity {
    validity_start: string;
    validity_end: string;
}

// error classes for determining type of error in express middleware error handler
export class ErrorNotFound extends Error {}
export class ErrorInternal extends Error {}

async function callLitackaApi(
    cardId: string,
    endpoint: string,
) {
    // Contruction of the URL based on the card ID and endpoint
    const url = new URL(String(configValues.litackaApiUrl));
    url.pathname += `${cardId}/${endpoint}`;

    const headers: HeadersInit = { 'Content-Type': 'application/json', 'X-API-Key': configValues.litackaApiKey };

    let response;
    try {
        // Fetch the data from the API using the constructed URL and headers
        response = await fetch(url, { method: 'GET', headers });
    } catch (error) {
        throw new ErrorInternal(`api call failed: ${error}`);
    }

    // Handling different response status codes
    switch (response.status) {
    case StatusCodes.OK:
        return response.json();
    case StatusCodes.NOT_FOUND:
        throw new ErrorNotFound();
    default:
        throw new ErrorInternal(`api call unexpected response: ${response.status}`);
    }
}

// these two functions get the results from fetch call and return the wanted value
export async function getState(cardId: string):Promise<{state_description: string}> {
    const result: LitackaState = await callLitackaApi(cardId, 'state');

    if (result.state_description === undefined) {
        throw new ErrorInternal('getState response does not contain state');
    }

    return { state_description: result.state_description };
}

export async function getValidity(cardId: string):Promise<{validity_end: string}> {
    const result: LitackaValidity = await callLitackaApi(cardId, 'validity');

    if ((result as LitackaValidity).validity_end === undefined) {
        throw new ErrorInternal('getValidity response does not contain state');
    }

    return { validity_end: result.validity_end };
}
