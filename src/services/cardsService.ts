import { getState, getValidity, ErrorInternal } from './litackaApiService';

function convertDateToExpirationDateString(
    date: string,
    delimiter = '.',
): string {
    const [year, month, day] = date.split('T')[0].split('-').map(Number);
    const dateString = `${day}${delimiter}${month}${delimiter}${year}`;

    // regex for checking of format is wanted fomrat, including number range of numbers
    const regex = /^([1-9]|[12][0-9]|3[01])\.([1-9]|1[0-2])\.\d{4}$/;

    // checking if formatting is good
    if (!regex.test(dateString)) {
        throw new ErrorInternal('bad date conversion');
    }

    return dateString;
}

// call services for getting wanted properties
export async function getCardDetails(
    cardId: string,
): Promise<{validity_end: string, state_description: string }> {
    const [validity, state] = await Promise.all([
        getValidity(cardId),
        getState(cardId),
    ]);

    return {
        validity_end: convertDateToExpirationDateString(validity.validity_end),
        state_description: state.state_description,
    };
}
