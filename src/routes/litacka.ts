import express from 'express';
import getCard from '../controllers/cardController';
import getHealthcheck from '../controllers/healthcheckController';
import userAuthentificationMiddleware from '../middlewares/userAuthentificationMiddleware';
import errorHandlerMiddleware from '../middlewares/errorHandlerMiddleware';

// Subrouter for /healtcheck endpoint
const healthcheckRouter = express.Router();

healthcheckRouter.get('/healthcheck', getHealthcheck);

// subrouter for /card endpoint
const cardRouter = express.Router();

cardRouter.use(userAuthentificationMiddleware);
cardRouter.get('/card', getCard);
cardRouter.use(errorHandlerMiddleware);

// merge of routers
const router = express.Router();

router.use(healthcheckRouter);
router.use(cardRouter);

export default router;
