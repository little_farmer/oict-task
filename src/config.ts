import assert from 'node:assert';
import dotenv from 'dotenv';

dotenv.config();

interface Config {
    port: string;
    litackaApiUrl: string;
    litackaApiKey: string;
}

(function checkEnviromentVariables() {
    const enviromentVariables = ['PORT', 'LITACKA_API_ENDPOINT_URL', 'LITACKA_API_ENDPOINT_API_KEY'];

    for (const variable of enviromentVariables) {
        assert(typeof process.env[variable] === 'string', `Enviroment variable: ${variable} is not defined`);
    }
}());

export default {
    port: process.env.PORT,
    litackaApiUrl: process.env.LITACKA_API_ENDPOINT_URL,
    litackaApiKey: process.env.LITACKA_API_ENDPOINT_API_KEY,
} as Config;
