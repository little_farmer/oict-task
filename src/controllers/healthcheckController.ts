// Health check endpoint
import { Request, Response } from 'express';
import { StatusCodes } from 'http-status-codes';

// simple function for healthcheck, it just returns JSON with status
export default function getHealthcheck(req: Request, res: Response) {
    res.status(StatusCodes.OK).send({ status: 'up' });
}
