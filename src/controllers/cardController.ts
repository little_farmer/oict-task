import { NextFunction, Request, Response } from 'express';
import { StatusCodes } from 'http-status-codes';
import * as cardService from '../services/cardsService';

export default async function getCard(req: Request, res: Response, next: NextFunction) {
    // check if cardId parameter was sent with request
    const { cardId } = req.query;
    if (typeof cardId !== 'string' || cardId.length === 0) {
        res.status(StatusCodes.BAD_REQUEST).send({
            error_code: StatusCodes.BAD_REQUEST,
            error_description: 'cardId parameter not provided',
        });
        return;
    }

    // try catch for errors, because Express can't catch errors from promises in middleware
    let results;
    try {
        results = await cardService.getCardDetails(cardId);
    } catch (error) {
        next(error);
        return;
    }

    res.status(StatusCodes.OK).send(results);
}
