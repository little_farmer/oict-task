# OICT-TASK

## Description

This project serves as a fulfillment of a task from the OICT for the acceptance procedure

This application exposes 2 endpoints

1. `/healthcheck`
    - just responds with information if the application is running and responding to requests
2. `/card`
    - returns state and expiration date in custom format for Litacka card, with application defined API key, from ENV variable, for communication with official litacka API

It should use HTTPS, but i ommited this option for demonstration purposes 


## Requirements
- [Node.JS](https://nodejs.org/en) v18

## Installation
1. Clone the repository
    
    **HTTPS**
    ```bash
    git clone https://gitlab.com/little_farmer/oict-task.git
    ```

    **SSH**
    ```bash
    git clone git@gitlab.com:little_farmer/oict-task.git
    ```

2. Install dependencies
    ```bash
    cd oict-task
    npm install
    ```

3. Build application
    ```bash
    npm run build
    ```

4. Set enviroment variables

    **For demonstration purposes i added prefilled .env file for repository, normally i wouldn't do that**

    Snippet is from .env.example
    ```bash
    PORT=3000
    LITACKA_API_ENDPOINT_URL=""
    LITACKA_API_ENDPOINT_API_KEY=""
    ```

5. Start the server
    ```bash
    npm start
    ```

6. **(Optional)** Test the application

    ```bash
    npm test
    ```

## Usage

The API endpoints are documented below in link. All endpoints are relative to the root URL of the API.

**[Swagger](https://app.swaggerhub.com/apis/DAVIDLITTLEFARMER_2/oict-task/1.0.0)**

**[YAML file](https://gitlab.com/little_farmer/oict-task/-/blob/main/api/api.yaml)**

You can try run request from terminal to check if server is running

```bash
curl -X 'GET' \
--header "Content-Type: application/json; charset=utf-8" \
http://localhost:3000/healthcheck
```

You should get back this response
```js
{"status":"up"}
```

Or try call this card
```bash
curl -X 'GET' \
--header "Content-Type: application/json; charset=utf-8" \
--header "X-Api-Key: 6spr9fydoy9eqknchzeckezy33d74tggiihp" \
'http://localhost:3000/card?cardId=9203111020153687'
```

You should get back this response
```js
{"card_status":"Aktivní v držení klienta","expiration_date":"12.8.2020"}
```